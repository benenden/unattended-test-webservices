# Unattended Test (Web Services) #

Develop an ASP.NET application using the latest .NET Core SDK

Expose two RESTful services:

1. Return a list of all Member resources
2. Return a Member resource for a given unique memberNumber

The basic example of a Member resource is:

```json
{
  "memberNumber":"1234567890",
  "forename":"Fred",
  "surname":"Smith",
  "products":[
    {
      "name":"Healthcare",
      "cost":100
    },
    {
      "name":"Travel Insurance",
      "cost":150
    }
  ]
}
```

Within the application the data source can be a hardcoded 'mock', or if you feel you have time use an in-memory or lightweight database technology.

The application should have an appropriate level of test coverage in the test framework of your choosing.

In the root directory of you application there should be a README file containing the following:

* Any assumptions you made
* How to build your application
* How to test your application
* How to run your application

You can submit your project in one of two ways:

1. Commit your project to a public git repository (github, bitbucket etc) and email the URL
2. Zip the project and email it
